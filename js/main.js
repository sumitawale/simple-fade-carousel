//document ready
jQuery (function(){
    initSlider();
});

//initiating Slider
function initSlider() {

    var options = {
        classSelector   : ".",
        sliderBlock     : "slider",
        classActive     : "js-active",
        slide           : "slide",
        btnPrev         : "btn-prev",
        btnNext         : "btn-next",
        btnDisable      : "js-btn-disable",
        pagination      : "pagination"
    };

    var selector = options.classSelector;
    var sliderBlock = jQuery(selector + options.sliderBlock); //Select slider holder
    var classActive = options.classActive; //Active class on slides
    var disable = options.btnDisable; //Disable class for buttons
    var slide = options.classSelector + options.slide; //Slides
    var btnPrev = options.classSelector + options.btnPrev; //Previous Button
    var btnNext = options.classSelector + options.btnNext; //Next Button
    var pagination = options.pagination;

    //Each slider block
    sliderBlock.each(function() {
        var self = jQuery(this);
        var selfSlide = self.find(slide);
        var selfBtnPrev = self.find(btnPrev);
        var selfBtnNext = self.find(btnNext);

        //Add active class on first slide
        selfSlide.first().addClass(classActive);

        //Fade Effect
        function fadeEffect(slideElement) {
            slideElement.css('opacity', 0);
            self.find(selector + classActive).animate({opacity: 1}, 600);
        }

        //Previous slide
        function slidePrev(element) {
            element.prev().addClass(classActive);
            element.removeClass(classActive);
            fadeEffect(selfSlide);
        }

        //Next slide
        function slideNext(element) {
            element.removeClass(classActive);
            element.next().addClass(classActive);
            fadeEffect(selfSlide);
        }

        //Disable button
        function disableButton(slideElement, btnElement) {
            if(slideElement.hasClass(classActive)) {
                btnElement.addClass(disable);
            }else {
                btnElement.removeClass(disable);
            }
        }

        //initiating disable button function
        disableButton(selfSlide.first(), selfBtnPrev);

        //initiating button previous function
        selfBtnPrev.on('click', function(e) {
            e.preventDefault();
            slidePrev(self.find(selector + classActive));
            selfBtnNext.removeClass(disable);
            disableButton(selfSlide.first(), jQuery(this));
        });

        //initiating button next function
        selfBtnNext.on('click', function(e) {
            e.preventDefault();
            slideNext(self.find(selector + classActive));
            selfBtnPrev.removeClass(disable);
            disableButton(selfSlide.last(), jQuery(this));
        });

        //Adding pagination
        function initPagination() {
            var list = '<ul class=' + pagination + '></ul>';
            self.append(list);

            selfSlide.each(function(index) {
                var listLink = '<span class="pagination-dots">' + index + '</span>';
                var listItem = '<li class="pagination-link">' + listLink + '</li>';
                self.find(selector + pagination).append(listItem);
            });
            paginationFlow();
        }

        //changing pagination
        function paginationFlow() {
            var pgListItem = self.find(selector + pagination + "-link");

            function changePagination() {
                var paginationSelf = jQuery(this);
                var getPaginationIndex = paginationSelf.index();
                selfSlide.siblings().removeClass(classActive);
                selfSlide.eq(getPaginationIndex).addClass(classActive);
                paginationSelf.siblings().removeClass(classActive);
                paginationSelf.addClass(classActive);
                fadeEffect(selfSlide);
                disableButton(selfSlide.first(), selfBtnPrev);
                disableButton(selfSlide.last(), selfBtnNext);
            }

            pgListItem.on('click', changePagination);
        }
        initPagination();

        //adding active class on pagination
        self.find(selector + pagination + '-link').first().addClass(classActive);
    });
}